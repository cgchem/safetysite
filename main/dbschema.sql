DROP TABLE IF EXISTS hph_dict;
DROP TABLE IF EXISTS chemicals;
DROP TABLE IF EXISTS nametocid;

CREATE TABLE hph_dict (
  code TEXT UNIQUE PRIMARY KEY,
  description TEXT NOT NULL,
  needs_so INTEGER
);

CREATE TABLE chemicals (
  cid INTEGER PRIMARY KEY,
  hphs TEXT,
  mw FLOAT,
  densities TEXT,
  cas TEXT
);

CREATE TABLE nametocid (
  name TEXT PRIMARY KEY,
  cid INTEGER
);