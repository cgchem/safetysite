import requests
from .regexes import *
from . import localdb


# module for looking up various properties based on a compound's CID, and a function to find a CID by searching
# the compound's name or other identifier.

CID_URL = "https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/data/compound/{cid}/JSON/?response_type=display"
LOOKUP_URL = '''https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/cids/txt'''
NAME_TO_CIDS_COMP = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/cids/txt"
NAME_TO_CIDS_COMP_WITH_NAME = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/property/IUPACName/json"
# you get the CID for free when the return type is JSON
NAME_TO_CIDS_SUBST = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/substance/name/cids/txt"
CID_TO_NAME = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/property/iupacname/txt"


def cid_to_json(cid):

    """returns the JSON Record object from a PubChem lookup to be passed to other
    functions to extract specific data. Get the cid using the name to cid function
    or by already knowing it."""

    resp = requests.get(CID_URL.format(cid=cid))
    js = resp.json()
    return js["Record"]
    # all PubChem entries have the same structure, everything within Record is laid
    # out the same way so good for recursive parsing etc


def find_section(lst, heading_type, target, key):

    """look in *list* under *heading type* and return all
    entries where *key* matches the *target* value"""

    out = []
    for x in lst[heading_type]:
        if x[key] == target:
            out.append(x)
    return out


def find_path(myjs, list_of_tuples):

    """takes a PubChem JSON page for a compound and returns the information at the location
    in the tree specified by the list of address tuples. After this the result is handed off
    to more specialised functions that can deal with the various different structures
    the data is stored in."""

    current = [myjs]  # the list we are currently looking at
    found = []  # append the entries we found matching the list_of_tuples entry

    for address in list_of_tuples:
        for y in current:  # look in all the things we found last cycle
            result = find_section(y, *address)
            if result:
                found.extend(result)  # extend, because result is a list
        current = found  # now search the next layer down, etc.
        found = []
    return current  # reached the bottom of the address list


def parse_ghs_section(adict):

    """Pull out all the strings from a GHS section"""

    # first we only want non-empty strings
    filt = filter(lambda x: x["String"] is not "", adict["Value"]["StringWithMarkup"])
    # then we want the actual value of the string
    return map(lambda x: x["String"], filt)


def get_ghs_sections(js):

    """Returns a list of parsed GHS sections after looking up a compund
    on PubChem via cid"""

    path = [('Section', 'Safety and Hazards', 'TOCHeading'),
            ('Section', 'Hazards Identification', 'TOCHeading'),
            ('Section', 'GHS Classification', 'TOCHeading'),
            ('Information', 'GHS Hazard Statements', 'Name')
            ]

    ghs_sections = find_path(js, path)
    return [parse_ghs_section(x) for x in ghs_sections]


@localdb.name_lookup_decorator
def name_to_cid(name):

    """Function to retrieve the CID based on any identifier deposited by a PubChem contributor.
    This might be a trivial or IUPAC name, CAS no. EC no. etc..."""

    a = requests.post(NAME_TO_CIDS_COMP, data=f"name={name}")
    if a:
        cid_list = a.text.split("\n")  # in 99% of cases a single CID will be returned, but splitting the list
        return cid_list[0]  # deals with the very rare occasions where there are multiple CIDs
    else:
        # no luck searching in the CID database, let's see if a "substance" has been deposited with this name
        b = requests.post(NAME_TO_CIDS_SUBST, data=f"name={name}")
        if not b:
            return None
        cid_list = b.text.split("\n")
        uniq = set(cid_list)  # get the unique entries in the list
        # count up the different CIDs and return only the most popular one
        counts = [(x, cid_list.count(x)) for x in uniq]
        counts.sort(key=lambda x: x[1])
        return counts[-1][0]


def rate_ghs_section(als):

    """returns the product of the number of companies and the number of notifications,
    so GHS sections with info from many more companies are favoured"""

    score = 0
    for x in als:
        qb = AGG_FINDER.findall(x)
        if qb:
            first, second = qb[0]  # findall returns a list that contains a tuple
            first = int(first)
            second = int(second)
            score = first * second
    return score


def sorted_ghs_section(als):

    """Sorts H-phrase list by percentage confidence, so the higher percentage ones come first"""

    def get_percentage(astr):

        ik = PCT_FINDER.search(astr)
        if ik:
            return float(ik.groups()[1])
        else:
            return 0

    out = []
    for x in als:
        if FINDER.search(x):
            out.append(x)
    out.sort(key=get_percentage)
    out.reverse()
    return out


@localdb.cid_lookup_decorator
def get_all_info(cid):

    """Returns a tuple of (H-phrases, molecular weight, density) for the given CID"""

    js = cid_to_json(cid)
    safe = get_safety_info(js)
    mwt = get_molweight(js)
    dens = get_density(js)
    cas = get_cas(js)
    dens2 = []
    for x in dens:
        p, q = x
        p = p.encode("Windows-1252").decode("UTF-8")  # necessary to deal with the degree symbol
        dens2.append((p, q))

    return safe, mwt, dens2, cas


def get_safety_info(js):

    sections = [[q for q in y] for y in get_ghs_sections(js)]
    # need to unpack the map objects here because they are consumed by
    # the rating function but are still needed later

    if not sections:
        return []
    if sections[0][0] == "Not Classified":
        #  no H-phrases, PubChem returns a standard response for this that we can
        #  show the user
        return [sections[0][1]]
    mapt = map(rate_ghs_section, sections)
    sorting_key = len  # by default choose the entry with most h-phrases reported
    for x in mapt:
        if x > 0:
            # if at least one section has aggregation information, use that instead
            sorting_key = rate_ghs_section
    sections.sort(key=sorting_key)
    to_ret = sorted_ghs_section(sections[-1])

    to_ret_2 = []
    for i in to_ret:
        so_phrase = SO_FINDER.search(i)
        if so_phrase:
            hph = so_phrase.group()
            if hph in NEEDS_SO:
                to_ret_2.append(i + " (requires signoff)")
            else:
                to_ret_2.append(i)

    return to_ret_2


def get_pastable_string(als):

    """take a list of H-phrase strings and compile a nicely copy-pastable string of
    H304, H314, H319... if percentage confidences are supplied, only take phrases
    with >45% confidence. If no percentages are supplied, fall back to using every phrase."""

    out = []
    backup = []
    for x in als:
        pl = PCT_FINDER.search(x)
        if pl:
            hph, percentage = pl.groups()
            if float(percentage) > 45.0:
                out.append(hph)
        else:
            ph = FINDER.search(x)
            if ph:
                backup.append(ph.group())
    if not out:
        return ", ".join(backup)
    else:
        return ", ".join(out)


def get_synonyms(js):

    stringlist = find_path(js, [
        ('Section', 'Names and Identifiers', 'TOCHeading'),
        ("Section", "Synonyms", "TOCHeading"),
        ('Section', 'Depositor-Supplied Synonyms', 'TOCHeading'),
    ])[0]["Information"][0]["Value"]["StringWithMarkup"]

    return [x["String"].capitalize() for x in stringlist]


def get_molweight(js):

    """Returns a single value (the computed pubchem value)"""

    path = [('Section', 'Chemical and Physical Properties', 'TOCHeading'),
            ('Section', 'Computed Properties', 'TOCHeading'),
            ('Section', 'Molecular Weight', 'TOCHeading')]

    info = find_path(js, path)[0]["Information"][0]["Value"]["Number"][0]

    return info


def get_density(js):

    """Returns a list of tuples of (value, reference)"""

    path = [('Section', 'Chemical and Physical Properties', 'TOCHeading'),
            ('Section', 'Experimental Properties', 'TOCHeading'),
            ('Section', 'Density', 'TOCHeading')]

    pathed = find_path(js, path)
    if len(pathed) > 0:
        info = pathed[0]["Information"]
    else:
        # not everything has a reported density
        return []

    out = []
    for x in info:
        try:
            ref = x["Reference"][0]
        except KeyError:
            continue
        val = x["Value"]["StringWithMarkup"][0]["String"]
        out.append((val, ref))
    return out


def get_cas(js):

    path = [('Section', 'Names and Identifiers', 'TOCHeading'),
            ('Section', 'Other Identifiers', 'TOCHeading'),
            ('Section', 'CAS', 'TOCHeading')]

    pathed = find_path(js, path)
    return pathed[0]["Information"][0]["Value"]["StringWithMarkup"][0]["String"]
