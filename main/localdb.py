import sqlite3
from flask import current_app, g
from flask.cli import with_appcontext
import click
from .regexes import *


def init_app(app):

    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)


def get_db():

    """get a connection to the database"""

    if "db" not in g:
        g.db = sqlite3.connect("mydb.sqlite3")
        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):

    db = g.pop("db", None)

    if db is not None:
        db.close()


def init_db():

    """first time database setup"""

    db = get_db()
    with current_app.open_resource("dbschema.sql") as f:
        db.executescript(f.read().decode("UTF-8"))
    import_hphrases(db)


@click.command("init-db")
@with_appcontext
def init_db_command():

    init_db()
    click.echo("Initialized the database")


def import_hphrases(conn):

    """function to populate the H-phrase lookup dictionary"""

    with current_app.open_resource("hphlist.txt") as f:
        content = f.read().decode("UTF-8")
        for line in content.split("\n"):
            line = line.rstrip("\r\n")
            phrase, desc = line.split(": ")
            conn.execute('''INSERT INTO hph_dict (code, description) VALUES (?,?)''', (phrase, desc))

    with current_app.open_resource("needs_signoff.txt") as f:
        content = f.read().decode("UTF-8")
        for line in content.split("\n"):
            conn.execute('''UPDATE hph_dict SET needs_so = ? WHERE code = ?''', (1, line.rstrip("\r\n")))

    conn.commit()


def add_compound(conn, cid, hphs, mw, densities, cas):

    """takes information as-displayed in the web page and writes it to the db for local
    lookup next time rather than always going to PubChem"""

    conn.execute('''INSERT INTO chemicals (cid, hphs, mw, densities, cas)
                    VALUES (?, ?, ?, ?, ?)''', (cid, hphs, mw, densities, cas))
    conn.commit()


def add_synonym(conn, cid, name):

    """If a compound is successfully found by name, that name is entered against the CID for
    future reference, rather than having to resolve it every time."""

    conn.execute('''INSERT INTO nametocid (name, cid) VALUES (?, ?)''', (name, cid))
    conn.commit()


def lookup_by_cid(conn, cid):

    a = conn.execute('''SELECT hphs, mw, densities, cas FROM chemicals WHERE cid = ?''', (cid,))
    return a.fetchone()


def lookup_by_name(conn, name):

    """this function expects the db to have sqlite3.Row as the row factory, so that it can
    address the column by name to find the cid"""

    a = conn.execute('''SELECT cid FROM nametocid WHERE name = ?''', (name,))
    return a.fetchone()


def get_hphrase_meaning(conn, phrase):

    a = conn.execute('''SELECT description, needs_so FROM hph_dict WHERE code = ?''', (phrase,))
    res = a.fetchone()
    if not res["needs_so"]:
        return res["description"]
    else:
        return res["description"] + " (requires signoff)"


def name_lookup_decorator(func):

    """wraps the function name-to-CID function, checks first if we have it locally and if so
    just returns the result of the local lookup, rather than going to PubChem every time."""

    def inner(name):
        res = lookup_by_name(get_db(), name)
        if res:
            return res["cid"]
        else:
            nures = func(name)
            if nures:  # only add names that resolve to CIDs
                add_synonym(get_db(), nures, name)
            return nures
    return inner


def cid_lookup_decorator(func):

    """check if we already have the H-phrase data and return it from our local DB if we can
    rather than going to PubChem. The return value is a tuple of H-phrases, mwt, densities"""

    def inner(cid):
        res = lookup_by_cid(get_db(), cid)
        if res:
            return retrieve_h_phrases(res["hphs"]), res["mw"], retrieve_densities(res["densities"]), res["cas"]
        else:
            nures = func(cid)
            raw_hph, mw, raw_density, cas = nures
            add_compound(get_db(), cid, store_h_phrases(raw_hph), mw, store_densities(raw_density), cas)
            return nures
    return inner


def store_h_phrases(alist):

    print(alist)
    if not alist:
        return None
    out = ""
    for x in alist:
        phrase_and_pct = PCT_FINDER.search(x)
        phrase_only = FINDER.search(x)
        if phrase_and_pct:
            phrase, pct = phrase_and_pct.groups()
        elif phrase_only:
            phrase = phrase_only.group()
            pct = 100
        else:
            return "$$$" + alist[0]
        out += f"{phrase}${pct}|"

    return out[:-1]  # drop the last | character


def retrieve_h_phrases(astr):

    if astr is None:
        return []
    if astr[:3] == "$$$":
        # "not classified" string
        return [astr[3:]]
    out = []
    for i in astr.split("|"):
        phrase, pct = i.split("$")
        meaning = get_hphrase_meaning(get_db(), phrase)
        out.append(f"{phrase} ({pct}%): {meaning}")
    return out


def store_densities(alist):

    if not alist:
        return None
    out = ""
    for x in alist:
        i, j = x
        out += f"{i}${j}|"
    return out[:-1]


def retrieve_densities(astr):

    if astr is None:
        return []
    out = []
    for a in astr.split("|"):
        i, j = a.split("$")
        out.append((i, j))
    return out
