from . import hphlookup_new as hph
import json

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)


bp = Blueprint('lookup_page', __name__, url_prefix='')

@bp.route('/', methods=('GET', 'POST'))

def search():
    if request.method == 'POST':

        query = request.form['qry']
        error = None

        if not query:
            error = 'query is required.'
            # should never run because page requires text box is filled in

        cid = hph.name_to_cid(query)
        # attempt cid lookup first and only try to get more info if it succeeds
        if error is None:

            if not cid:
                session["imagelink"] = url_for("static",filename="notfound.png")
                session["pclink"] = None
                session["query"] = query
                session["result"] = [" "]
                session["restext"] = " "
                mw = den = 0
            else:

                session["query"] = query
                session.permanent = True  # conserve between browser closures
                res, mw, den, cas = hph.get_all_info(cid)

                session["imagelink"] = "https://pubchem.ncbi.nlm.nih.gov/image/" \
                                       "imagefly.cgi?cid={}&width=250&height=250".format(cid)
                session["pclink"] = "https://pubchem.ncbi.nlm.nih.gov/compound/{}".format(cid)
                session["mw"] = mw
                session["den"] = den
                session["cas"] = cas

                if not res:
                    # no H-phrases but we will always have a mw, maybe a density
                    session["result"] = [" "]
                    session["restext"] = "No H-phrases found"
                else:
                    session["result"] = res
                    session["restext"] = hph.get_pastable_string(res)

            return render_template('lookup_page/alookup.html',
                                   data=(json.dumps(session["restext"])),
                                   boxsize=len(session["restext"]),
                                   )

        flash(error)

    # need to set some default values to display stuff if the search didn't return anything
    if not "boxsize" in session:
        session["boxsize"] = 10
    if not "restext" in session:
        session["restext"] = " "
    return render_template('lookup_page/alookup.html',
                           data=(json.dumps(session["restext"])),
                           boxsize=len(session["restext"]))


@bp.route('/add_favourite')
def add_fave():

    if not "faves" in session:
        session["faves"] = {}
    else:
        if type(session["faves"]) is not dict:
            session["faves"] = {}

    session.modified=True
    # session can detect when its attributes are reassigned, but does not
    # detect changes in its mutable attributes
    session["faves"][session["query"]] = session["restext"]
    return redirect(url_for('lookup_page.search'))


@bp.route('/delete_favourite/<string:key>')
def del_fave(key):

    session.modified = True
    session["faves"].pop(key)
    return redirect(url_for('lookup_page.search'))