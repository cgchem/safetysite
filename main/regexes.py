"""Regex matching objects to extract H-phrases and associated data from PubChem pages"""

import re

FINDER = re.compile('''H[0-9]{3}''')
AGG_FINDER = re.compile('''Aggregated[a-zA-Z ]*([0-9]*)[a-zA-Z ]*([0-9]*)''')
PCT_FINDER = re.compile('''(H[0-9]{3}) \(([0-9. ]*)%\)''')
SO_FINDER = re.compile('''H[0-9]{3}[H203|iIFD\-7]*''')

NEEDS_SO = ["H200-H203", "H260", "H271", "H300", "H304", "H310", "H317", "H330", "H334", "H340", "H350", "H350i",
            "H360F", "H360D", "H361F", "H361D", "H362", "H370-H373", "H371", "H372", "H373", "H201", "H202", "H200"]
# H-phrases requiring supervisor signoff in the group
